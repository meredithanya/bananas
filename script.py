import sentry_sdk
import os
version = os.environ.get("BITBUCKET_COMMIT", "1.0.0")
sentry_sdk.init(dsn='https://0084a66234134272bcde5eac27ed863e@sentry.io/115550', release=version)


try: 
    boop()
except Exception as err:
    sentry_sdk.capture_exception(err)