#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

info "Executing the pipe..."

# Default parameters
DEBUG=${DEBUG:="false"}

run curl -X PUT "https://api.bitbucket.org/internal/repositories/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/commit/${BITBUCKET_COMMIT}/reports/"

